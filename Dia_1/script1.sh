#!/bin/bash

lineas=$(wc -l puzzle.txt | awk '{print $1}')
SOLUCION=0

for ((i=2 ; i<=lineas ; i++))
            do
            j=`expr $i - 1`
            DATO1=$(awk -v linea=$i 'FNR == linea' puzzle.txt)
            DATO2=$(awk -v linea=$j 'FNR == linea' puzzle.txt)
            if [ $DATO1 -gt $DATO2 ]
                then
                SOLUCION=`expr $SOLUCION + 1`
            fi
        done
echo "la solución es: $SOLUCION"

