#!/bin/bash

lineas=$(wc -l puzzle.txt | awk '{print $1}')
SOLUCION=0

for ((i=1 ; i<=lineas-3 ; i++))
            do
            j=`expr $i + 1`
            k=`expr $i + 2`
            DATO1=$(awk -v linea=$i 'FNR == linea' puzzle.txt)
            DATO2=$(awk -v linea=$j 'FNR == linea' puzzle.txt)
            DATO3=$(awk -v linea=$k 'FNR == linea' puzzle.txt)
            SUMA1=`expr $DATO1 + $DATO2 + $DATO3`
            a=`expr $i + 1`
            b=`expr $i + 2`
            c=`expr $i + 3`
            DATO11=$(awk -v linea=$a 'FNR == linea' puzzle.txt)
            DATO22=$(awk -v linea=$b 'FNR == linea' puzzle.txt)
            DATO33=$(awk -v linea=$c 'FNR == linea' puzzle.txt)
            SUMA2=`expr $DATO11 + $DATO22 + $DATO33`
            if [ $SUMA2 -gt $SUMA1 ]
                then
                SOLUCION=`expr $SOLUCION + 1`
            fi
        done
echo "la solución es: $SOLUCION"

