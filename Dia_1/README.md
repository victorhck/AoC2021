# Dia 1

## Primer reto

En este primer reto, del archivo puzzle.txt hay que averiguar qué número de la lista es mayor que el anterior y contar todos los números que son mayores que el número anterior

*Ejemplo*

199 (N/A)
200 (increased)
208 (increased)
210 (increased)
200 (decreased)
207 (increased)
240 (increased)
269 (increased)
260 (decreased)
263 (increased)

## Segundo reto

De la misma lista de números de puzzle.txt hay que sumar los números en grupos de 3 desde el primero y después de esas diferentes sumas saber qué número es mayor que el anterior y contarlos

*Ejemplo*

199  A
200  A B
208  A B C
210    B C D
200  E   C D
207  E F   D
240  E F G
269    F G H
260      G H
263        H
