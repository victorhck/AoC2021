# Dia 2

## Primer reto

el puzzle da las indicaciones de hacia adelante arriba y abajo de un submarino:

* forward X incremente la posición horizontal en X unidades.
* down X incremente la profundidad en X unidades.
* up X disminuye la profundidad en X unidades.

Siguiendo las indicaciones del puzzle, dar el valor final del resultado que será la multiplicación de la posición por la profundidad.

*Ejemplo*

forward 5
down 5
forward 8
up 3
down 8
forward 2

Posición 15, profundidad 10, lo que es igual a 150

## Segundo reto

Junto con la posición y profundidad ahora también hay que calcular el objetivo (aim) de la siguiente manera.

* down X incremente el objetivo en X unidades.
* up X disminuye el objetivo en X unidades.
* forward X realiza dos cosas:
** Incrementa la posición en X unidades.
** Incrementa la profundidad multiplicándolo por X.

*Ejemplo*

forward 5
down 5
forward 8
up 3
down 8
forward 2

Posición 15 profundidad 60 lo que hace un total de 900


