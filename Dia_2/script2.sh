#!/bin/bash

lineas=$(wc -l puzzle.txt | awk '{print $1}')
adelante=forward
arriba=up
abajo=down
objetivo=0

for ((i=1 ; i<=lineas ; i++))
    do
        comando=$(awk -v linea=$i 'FNR == linea {print $1}' puzzle.txt)
        numero=$(awk -v linea=$i 'FNR == linea {print $2}' puzzle.txt)
        if [ $comando == $adelante ]
            then
            posicion=`expr $posicion + $numero`
            objetivo=`expr $profundidad \* $numero`
            objfinal=`expr $objfinal + $objetivo`
        fi
        if [ $comando == $arriba ]
            then
            profundidad=`expr $profundidad - $numero`
        fi
        if [ $comando == $abajo ]
            then
            profundidad=`expr $profundidad + $numero`
        fi
        #echo "pos $posicion prof $profundidad obje $objetivo"       
    done

resultado=`expr $posicion \* $objfinal`
echo "la suma es: $resultado"

