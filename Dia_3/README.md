# Dia 3

## Primer reto

De una lista de números binarios, consiste en calcular el valor *gamma* y *epsilon*.
El valor *gamma* se calcula con el valor más común de cada bit de cada número binario del puzzle.

*Ejemplo*

00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010

El valor *gamma*, se calcula con en cada primer bit de cada número, si el valor más común es 1 el valor de gamma del primer bit será 1. etc

El valor gamma del ejemplo será 10110 lo que es 22 en decimal.

El valor epsilon es el valor menos común de cada bit de cada palabra, lo que será en el ejemplo 01001 que es 9 en decimal.

La primera solución será multiplicar ambos números. En el ejemplo: 22 * 9 =198
